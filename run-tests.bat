:: Copyright 2010-2012 Canonical Ltd.
::
:: This program is free software: you can redistribute it and/or modify it
:: under the terms of the GNU Lesser General Public License version 3,
:: as published by the Free Software Foundation.
::
:: This program is distributed in the hope that it will be useful, but
:: WITHOUT ANY WARRANTY; without even the implied warranties of
:: MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
:: PURPOSE.  See the GNU General Public License for more details.
::
:: You should have received a copy of the GNU General Public License along
:: with this program.  If not, see <http://www.gnu.org/licenses/>.
@ECHO off

SET PYTHONEXEPATH=""
SET PYTHONPATH=.

ECHO Checking for Python on the path
:: Look for Python from buildout
FOR %%A in (python.exe) do (SET PYTHONEXEPATH=%%~$PATH:A)
FOR %%C in (pyflakes) do (SET FLAKESPATH=%%~$PATH:C)
FOR %%D in (pep8.exe) do (SET PEP8PATH=%%~$PATH:D)

IF NOT "%PYTHONEXEPATH%" == "" GOTO :PYTHONPRESENT

ECHO Please ensure you have python installed
GOTO :END

:PYTHONPRESENT
ECHO Python found, executing the tests...
:: execute the tests with a number of ignored linux only modules
"%PYTHONEXEPATH%" setup.py build test clean
"%PYTHONEXEPATH%" "%FLAKESPATH%" dirspec
:: test for style if we can, if pep8 is not present, move to the end
IF EXIST "%PEP8PATH%" (
"%PEP8PATH%" --repeat dirspec
)ELSE (
ECHO Style checks were not done)
:END
