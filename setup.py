#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2011-2012 Canonical Ltd.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Setup and install dirspec."""
from setuptools import setup

setup(name='dirspec',
      version='99.12',
      description='XDG Base and User directories implementation',
      license='GNU LGPL v3',
      url='https://launchpad.net/dirspec',
      download_url='https://launchpad.net/dirspec/+download',
      packages=['dirspec'],
      test_suite='dirspec.tests',
      )
